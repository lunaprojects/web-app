import React from 'react'
import { Layout, Breadcrumb } from 'antd'

const { Content } = Layout

class AppContent extends React.Component {
    constructor(props){
        super(props)
    }

    render () {
        return (
            <Content style={{ padding: '0 50px', minHeight: '100vh'}}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item>Home</Breadcrumb.Item>
                </Breadcrumb>
                <div style={{
                    minHeight: '100%',
                    borderRadius: '5px' }}>
                    {this.props.children}
                </div>
            </Content>
        )
    }
}

export default AppContent