import React from 'react'
import Link  from 'next/link'
import Router from 'next/router'
import { Layout, Menu, Icon } from 'antd'
import '../../static/css/layout.css'

const { Sider } = Layout

class AppMenu extends React.Component {

    constructor(props){
        super(props)

        this.state = {
            collapsed : !true,
        }
    }

    render () {
        return (
            <Sider width={200} style={{ height: '100vh'}}>
                <Menu
                    className="main-sider"
                    mode="inline"
                    style={{ height: '100%', borderRight: 0 }}
                    >
                    <Menu.Item key="1" onClick={() => Router.push('/')}>
                        <Icon type="euro" theme="outlined"/>
                        <span><a>Home</a></span>
                    </Menu.Item>
                    <Menu.Item key="2" onClick={() => Router.push('/uc/expenses')}>
                        <Icon type="pie-chart" theme="outlined" />
                        <span><a>Expenses</a></span>
                    </Menu.Item>
                </Menu>
            </Sider>
        )
    }
}

export default AppMenu