import React from 'react'
import Router from 'next/router'
import { Menu, Icon } from 'antd'

class UserCard extends React.Component {
    constructor(props){
        super(props)
    }

    render() {
        return (
            <Menu className="menu-header" mode="horizontal">
                <Menu.SubMenu
                    title={
                        <span className="submenu-title-wrapper">
                            <Icon type="robot" theme="outlined" />
                                {this.props.fullname}
                        </span>
                    }
                    >
                    <Menu.ItemGroup title="Personal data">
                        <Menu.Item key="setting:1">Name: <strong>{this.props.fullname} </strong></Menu.Item>
                        <Menu.Item key="setting:2">Username: <strong>{this.props.username} </strong></Menu.Item>
                        <Menu.Item key="setting:3">Email: <strong>{this.props.email} </strong></Menu.Item>
                        <Menu.Item key="setting:4" onClick={() => Router.push('/logout')}>
                            <Icon type="logout" theme="outlined" onClick={() => Router.push('/logout')}/>
                            <span><a style={{ textDecoration: 'none', width: '100%'}}>Logout</a></span>
                        </Menu.Item>
                    </Menu.ItemGroup>
                </Menu.SubMenu>
            </Menu>
        )
    }
}

export default UserCard