import React from 'react'
import { Layout } from 'antd'
import { connect } from 'react-redux'
import { fetchMenus } from '../store/actions/menu'
import UserCard from './assets/UserCard'
import AppMenu from './assets/AppMenu'
import AppContent from './assets/AppContent'


const { Header, Footer } = Layout

class AppLayout extends React.Component {

    render(){
        const { auth, menu } = this.props

        if (!!auth.status){
            return (
                <Layout className="main" style={{ height: '100vh'}}>
                    <Header className="main-header">
                        <span style={{ color: "#FF6C60", fontSize: "20px" }}>D</span>
                        <span style={{ color: "black",fontSize: "20px" }}>LUNA</span>
                        <span>&reg;</span>
                        <UserCard {...auth} />
                    </Header>
                    <Layout className="main">
                        <AppMenu {...menu} />
                        <Layout>
                            <AppContent {...this.props} />
                        </Layout>
                    </Layout>
                </Layout>
            )
        }
        return (
            <Layout style={{ minHeight: '100vh', backgroundColor: '#1b1b1b'}}>
                {this.props.children}
            </Layout>
        )

    }
}

const mapDispatchToProps = dispatch => {
    return {
        getMenus: dispatch(fetchMenus())
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth,
        menu: state.menu
    }
}
export default connect(mapStateToProps)(AppLayout)