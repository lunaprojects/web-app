const path = require('path')
const http = require('http')
const express = require('express')
const next = require('next')
const bodyParser = require('body-parser')
const flash = require('connect-flash')
const session = require('express-session')
const RedisStore = require('connect-redis')(session)
const dotenv = require('dotenv')
const auth = require('./auth')

dotenv.config({path: path.join(__dirname, './.env')})

const port = parseInt(process.env.PORT, 10)
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

const sess = {
  name: process.env.SESSION_NAME,
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: true,
  store: new RedisStore({
      host: process.env.REDIS_HOST,
      port: process.env.REDIS_PORT,
      pass : process.env.REDIS_PWD,
      ttl: process.env.REDIS_TTL,
      db: Number(process.env.REDIS_DB)
  })
}

app.prepare()
  .then(() => {
    const server = express()
    const httpServer = http.createServer(server)

    server.use(bodyParser.json())
    server.use(bodyParser.urlencoded({ extended: true}))
    server.use(session(sess))
    server.use(flash())
    auth(server)
    server.get('/', (req, res) => {
      return app.render(req, res, '/index', req.query)
    })

    server.get('*', (req, res) => {
      return handle(req, res)
    })

    httpServer.listen(port, (err) => {
      if (err) throw err
      console.log(`> Ready on http://localhost:${port}`)
    })
  })
