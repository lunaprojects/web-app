'use strict'

const passport = require('passport')
const passportLocal = require('passport-local')
const axios = require('axios')


module.exports = function auth(server){
    const Strategy = passportLocal.Strategy
    const verify = async (req, username, password, done) => {
        try {
            const res = await axios.post('http://columnstore:5000/api/auth',
                {username: username, password: password}, {timeout:1000})
            const data = res.data
            return done(null, data)
        }
        catch (error) {
            if (typeof error.response != 'undefined'){
                const message = error.response.data['message']
                return done(null, false, req.flash('LOGIN_STATE', message))
            }
            return done(null, false, req.flash('LOGIN_STATE', "Algo ha ido mal!."))
        }
    }

    passport.use(new Strategy(
        {
          usernameField: 'username',
          passwordField: 'password',
          passReqToCallback: true
        },
        verify
      ))

    passport.serializeUser((user, done) => {
        return done(null, user)
    })

    passport.deserializeUser((user, done) => {
        return done(null, user)
    })

    server.use(passport.initialize())
    server.use(passport.session())

    server.post('/login',
        passport.authenticate('local', {failureRedirect: '/login'}),
            (req, res) => {
                res.redirect('/')
            }
    )

    server.get('/logout',
        (req, res) => {
            req.logout()
            res.redirect('/login')
        }
    )
}
