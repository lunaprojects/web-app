import { FETCH_MENU_BEGIN, FETCH_MENU_SUCCESS, FETCH_MENU_FAILURE } from '../constants'


let initState = {}

export default (state = initState, action) => {
    if (action.type == FETCH_MENU_BEGIN || action.type == FETCH_MENU_SUCCESS || action.type == FETCH_MENU_FAILURE ){
        return {
            ...state,
            ...action.payload
        }
    }
    return state
}