'use strict'

import { SET_AUTH_USER } from '../constants'

let initState = {}

export default (state = initState, action) => {
    if (action.type == SET_AUTH_USER) {
        return {
            ...state,
            ...action.payload
        }
    }
    return state
}