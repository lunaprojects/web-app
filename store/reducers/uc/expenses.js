import { F_EXP_BEGIN, F_EXP_SUCCESS, F_EXP_FAILURE } from '../../constants'

export default (state = {}, action  ) => {
    if (action.type == F_EXP_BEGIN || action.type == F_EXP_SUCCESS || action.type == F_EXP_FAILURE  ) {
        return {
            ...state,
            ...action.payload
        }
    }
    return state
}