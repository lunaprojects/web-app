'use strict'

import { combineReducers } from 'redux'
import auth from './auth'
import expenses from './uc/expenses'
import menu from './menu'
//import users from './api/users'

export default combineReducers({ auth, expenses, menu })
