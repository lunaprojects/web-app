import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import reducers from './reducers'

export function initializeStore (initialState = {}) {

    //const middleware = [thunk.withExtraArgument({api})]
    let enhancer
    enhancer = process.env.NODE_ENV === 'development' ? composeWithDevTools(applyMiddleware(thunk)) : compose(applyMiddleware(thunk))
    return createStore(reducers, initialState, enhancer)
}