'use strict'

import axios from 'axios'
import { FETCH_MENU_BEGIN, FETCH_MENU_SUCCESS, FETCH_MENU_FAILURE } from './../constants'


export const fetchMenuBegin = () => ({type: FETCH_MENU_BEGIN})

export const fetchMenuSuccess = ( menus ) => ({
    type: FETCH_MENU_SUCCESS,
    payload: {
        ...menus
    }
})


export const fetchMenuError = ( error ) => ({
    type: FETCH_MENU_FAILURE,
    payload: {
        ...error
    }
})

export const fetchMenus = () => {
    return async (dispatch, getState) => {
        dispatch(fetchMenuBegin())
        const nowState = getState()
        const token = nowState.authentication ? nowState.authentication.access_token : null
        try {
            const res = await axios.get('http://devdrop:5000/api/menu',{
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            })
            const data = res.data
            dispatch(fetchMenuSuccess(data))
        }
        catch (error) {
            dispatch(fetchMenuError(error))
        }
    }
}