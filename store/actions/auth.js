'use strict'

import { SET_AUTH_USER } from '../constants'

export const setAuthUser = user => ({
    type: SET_AUTH_USER,
    payload: {
        url: 'http://daluna.me:5000/api',
        ...user
    }
})
