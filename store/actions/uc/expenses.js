/***
 * Reducer setter states for actions getting data from database
 * @author David Luna
 *
 */
import axios from 'axios'
import { F_EXP_BEGIN, F_EXP_SUCCESS, F_EXP_FAILURE } from '../../constants'
//const API_URL =  process.env.API_URL

/***
 * init state action
 */
const getDataInit = () => {
    return {
        type: F_EXP_BEGIN
    }
}

const getDataSuccess = (expenses) => {
    return {
        type: F_EXP_SUCCESS,
        payload: {
            ...expenses
        }
    }
}

const getDataFailure = (error) => {
    return {
        type: F_EXP_FAILURE,
        payload: {
            ...error
        }
    }
}

export const fetchExpensesData = () => {
    return async (dispatch, getState) => {
        dispatch(getDataInit())
        const nowState = getState()
        const token = nowState.authentication ? nowState.authentication.access_token : null
        try {
            const res = await axios.get('http://columnstore:5000/api/expenses' , {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            })
            const data = await res.data
            return dispatch(getDataSuccess(data))
        }
        catch (error) {
            return dispatch(getDataFailure(error))
        }
    }
}