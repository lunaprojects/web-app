import React from 'react'
import { Table, Row, Col,Upload, message, Button, Icon, Card } from 'antd'
import { connect } from 'react-redux'
import withAuth from '../../lib/with-auth'
import { fetchExpensesData }  from '../../store/actions/uc/expenses'
import '../../static/css/expenses.css'

class UnderControl extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            columns: [
                {
                    title: 'ID',
                    dataIndex: 'id'
                },{
                    title: 'Categoria',
                    dataIndex: 'category',
                    sorter: (a,b) => a.category.toLowerCase().localeCompare(b.category.toLowerCase())
                // specify the condition of filtering result
                // here is that finding the name started with `value`
                // onFilter: (value, record) => record.name.indexOf(value) === 0,
                // sorter: (a, b) => a.name.length - b.name.length,
                }, {
                    title: 'Fecha gasto',
                    dataIndex: 'date_at',
                    defaultSortOrder: 'descend',
                    render: (value) => new Date(value).toLocaleString('es-ES').split(' ')[0],
                    sorter: (a, b) => (new Date(b.date_at) - new Date(a.date_at))
                }, {
                    title: 'Amount',
                    dataIndex: 'amount',
                    render: (value) => parseFloat(value).toFixed(2) + '€',
                    sorter: (a, b) => a.amount - b.amount
                }, ]
        }
    }

    render () {
        // const propsP = {
        //     name: 'file',
        //     action: 'http://columnstore:5000/api/upload',
        //     headers: {
        //       authorization: 'authorization-text',
        //     },
        //     onChange(info) {
        //       if (info.file.status !== 'uploading') {
        //         console.log(info.file, info.fileList);
        //       }
        //       if (info.file.status === 'done') {
        //         message.success(`${info.file.name} file uploaded successfully`);
        //       } else if (info.file.status === 'error') {
        //         message.error(`${info.file.name} file upload failed.`);
        //       }
        //     },
        //   };
        const { expenses } = this.props

        return (
            <div>
                <Row gutter={24}>
                    <Card hoverable>
                        <Col span={2}>
                            <Button>
                                <Icon type="plus"/> New
                            </Button>
                        </Col>
                        {/* <Col span={2}>
                            <Upload {...propsP}>
                                <Button>
                                    <Icon type="upload"/> Upload file
                                </Button>
                            </Upload>
                        </Col> */}
                    </Card>
                </Row>
                <Row gutter={24} style={{ marginTop: '1%' }}>
                    <Card hoverable>
                        <Col span={12}>
                            <Table
                                size="middle"
                                title={() => "Under Control - Expenses"}
                                columns={this.state.columns}
                                dataSource={ expenses.data }
                                pagination={{ pageSize: 50 }}
                                scroll={{ y: 240 }}/>
                        </Col>
                        <Col span={12}>
                            {/* <ReactFC
                                {...expenses.charts}/> */}
                        </Col>
                    </Card>
                </Row>
          </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchExpensesData: dispatch(fetchExpensesData())
    }
}

const mapStateToProps = state => {
    return {
        expenses: state.expenses
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withAuth()(UnderControl))


