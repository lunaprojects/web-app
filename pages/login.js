'use strict'

import React from 'react'
import { message }  from 'antd'
import withAuth from '../lib/with-auth'
import '../static/css/login.css'


class Login extends React.Component  {
    static async getInitialProps (ctx) {
        let loginState
        if (!!ctx.req) {
            return {
                loginState: ctx.req.flash('LOGIN_STATE')
            }
        }
        return { loginState }
    }

    render() {
        const { loginState } = this.props
        return (
            <div>
                { loginState.length > 0 ? message.error(loginState, 5) : null }
                <div className="background-wrap">
                    <div className="background"></div>
                </div>
                <form id="accesspanel" action="login" method="POST">
                    <h1 id="litheader">DALUNA</h1>
                    <div className="inset">
                        <input type="text" name="username" id="username" placeholder="Username" />
                        <input type="password" name="password" id="password" placeholder="Password"/>
                        <input className="loginLoginValue" type="hidden" name="service" value="login" />
                    </div>
                    <p className="p-container">
                        <input type="submit" name="Login" id="go" value="Login"/>
                    </p>
                </form>
            </div>
        )
    }
}

export default withAuth({logoutRequired: true})(Login)