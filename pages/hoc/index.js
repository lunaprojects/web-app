import React from 'react'

const { Component } = React;

const overrideProps = (overrideProps) => (BaseComponent) => (props) =>
    <BaseComponent {...props} {...overrideProps} />

const neverRender = (BaseComponent) =>
    class extends Component {
        shouldComponentUpdate(){
            return false;
        }
        render(){
            return <BaseComponent {...this.props} />
        }
    }

const User = ({name}) =>
    <div className="User"> {name} </div>

const User2 = neverRender(User)

const App = () =>
    <div>
        <User name="Tim" />
        <User2 name="David"/>
    </div>

export default App