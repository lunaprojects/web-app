import React, { PureComponent } from 'react'
import Router from 'next/router'

import { setAuthUser } from '../store/actions/auth'
import { fetchMenus } from '../store/actions/menu'

let globalUser = null

export default ({ loginRequired = true, logoutRequired = false } = {}) => (BaseComponent) => {
  class App extends PureComponent {
    static async getInitialProps (ctx) {
      const isFromServer = !!ctx.req
      const user = ctx.req && ctx.req.user ? ctx.req.user : globalUser

      if (isFromServer && user) {
        user.id = user.id
        ctx.reduxStore.dispatch(setAuthUser(user.data))
        ctx.reduxStore.dispatch(fetchMenus())
      }

      const props = { user, isFromServer }

      if (BaseComponent.getInitialProps) {
        Object.assign(props, (await BaseComponent.getInitialProps(ctx)) || {})
      }

      return props
    }

    componentDidMount () {
      const { user } = this.props

      if (this.props.isFromServer) {
        globalUser = this.props.user
      }

      if (loginRequired && !logoutRequired && !user) {
        Router.push('/login')
        return
      }

      if (logoutRequired && user) {
        Router.push('/')
      }
    }

    render () {
      const { user } = this.props

      if (loginRequired && !logoutRequired && !user) {
        return null
      }

      if (logoutRequired && user) {
        return null
      }

      return <BaseComponent {...this.props} />
    }
  }

  App.defaultProps = {
    user: null
  }

  return App
}
